// This function will Find the number of times each team won the toss and also won the match.

function tossAndWin(matches) {
    let filteredArray = matches.filter((match) => (match.toss_winner == match.winner))
    let results = filteredArray.reduce((acc, cv) => {
        acc[cv.toss_winner] ? acc[cv.toss_winner] += 1 : acc[cv.toss_winner] = 1
        return acc
    }, {})
    return results
}

module.exports.tossAndWin = tossAndWin;

// This function will Find a player who has won the highest number of Player of the Match awards for each season.

function playerOfTheMatch(matches) {
    let result = {}, max = {};
    matches.map(function cb(match) {
        if (result[match.season]) {
            if (result[match.season][match.player_of_match]) {
                result[match.season][match.player_of_match] += 1
            } else {
                result[match.season][match.player_of_match] = 1
            }
        } else {
            result[match.season] = {}
            result[match.season][match.player_of_match] = 1
        }
    })
    for (let season in result) {
        let max_won = ["", 0]
        for (let player in result[season]) {
            if (result[season][player] > max_won[1]) {
                max_won[0] = player
                max_won[1] = result[season][player]
            }
        }
        max[season] = [max_won[0],max_won[1]]
    }
    return max
}

module.exports.playerOfTheMatch = playerOfTheMatch;

// This function will Find the strike rate of a batsman for each season.

function strikeRate(matches, deliveries) {
    let match_id = {}, strike_rate = {}
    matches.map(function cb(match) {
        if (!match_id[match.id]) {
            match_id[match.id] = match.season
        }
    })
    deliveries.map(function cb(delivery) {
        let year = match_id[delivery.match_id]
        if (strike_rate[delivery.batsman]) {
            if (strike_rate[delivery.batsman][year]) {
                strike_rate[delivery.batsman][year]['run'] += Number(delivery.batsman_runs)
                strike_rate[delivery.batsman][year]['ball'] += 1
                strike_rate[delivery.batsman][year]['strikeRate'] = Math.round(100 * strike_rate[delivery.batsman][year]['run'] / strike_rate[delivery.batsman][year]['ball'])

            } else {
                strike_rate[delivery.batsman][year] = {}
                strike_rate[delivery.batsman][year]['run'] = Number(delivery.batsman_runs)
                strike_rate[delivery.batsman][year]['ball'] = 1
                strike_rate[delivery.batsman][year]['strikeRate'] = Math.round(100 * strike_rate[delivery.batsman][year]['run'] / strike_rate[delivery.batsman][year]['ball'])
            }
        } else {
            strike_rate[delivery.batsman] = {}
            strike_rate[delivery.batsman][year] = {}
            strike_rate[delivery.batsman][year]['run'] = Number(delivery.batsman_runs)
            strike_rate[delivery.batsman][year]['ball'] = 1
            strike_rate[delivery.batsman][year]['strikeRate'] = Math.round(100 * strike_rate[delivery.batsman][year]['run'] / strike_rate[delivery.batsman][year]['ball'])
        }
    })
    return strike_rate
}

module.exports.strikeRate = strikeRate;

// This function will Find the highest number of times one player has been dismissed by another player.

function playerDismissed(deliveries) {
    let result = {}, max = ['', 0]
    deliveries.map(function cb(delivery) {
        if (delivery.player_dismissed != "") {
            let pair = delivery.player_dismissed + " dismissed by " + delivery.bowler
            if (result[pair]) {
                result[pair] += 1
            } else {
                result[pair] = 1
            }
        }
    })
    for (let pair in result) {
        if (result[pair] > max[1]) {
            max[0] = pair
            max[1] = result[pair]
        }
    }
    return { [max[0]]: max[1] }
}

module.exports.playerDismissed = playerDismissed;

// This function will Find the bowler with the best economy in super overs.

function economicBowlerInSuperOver(deliveries) {
    let result = {}, best = ['', Infinity]
    deliveries.map(function cb(delivery) {
        if (delivery.is_super_over == '1') {
            if (result[delivery.bowler]) {
                result[delivery.bowler]['runs'] += Number(delivery.total_runs)
                result[delivery.bowler]['ball'] += 1
                result[delivery.bowler]['economy'] = result[delivery.bowler]['runs'] / (result[delivery.bowler]['ball'] / 6)
            } else {
                result[delivery.bowler] = {}
                result[delivery.bowler]['runs'] = Number(delivery.total_runs)
                result[delivery.bowler]['ball'] = 1
                result[delivery.bowler]['economy'] = result[delivery.bowler]['runs'] / (result[delivery.bowler]['ball'] / 6)
            }
        }
    })
    for (let bowler in result) {
        if (result[bowler]['economy'] < best[1]) {
            best[0] = bowler
            best[1] = result[bowler]['economy']
        }
    }
    return { [best[0]]: best[1] }
}

module.exports.economicBowlerInSuperOver = economicBowlerInSuperOver;