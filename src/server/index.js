// All the required package
const fs = require('fs');
const csv = require('csvtojson')
const ipl = require('./ipl');
const path = require('path');
const http = require('http')

// All the file paths
const MATCHES_PATH = path.join(__dirname, '../data/matches.csv')
const DELIVERIES_PATH = path.join(__dirname, '../data/deliveries.csv')
const TEAMS_WINNING_TOSS_AND_MATCH = path.join(__dirname, '../public/output/teamsWinningTossAndMatch.json')
const PLAYER_OF_THE_MATCH = path.join(__dirname, '../public/output/maxPlayerOfTheMatch.json')
const STRIKE_RATE_EACH_YEAR = path.join(__dirname, '../public/output/strikeRateEachYear.json')
const PLAYER_DISMISSED = path.join(__dirname, '../public/output/playerDismissedMaxTimes.json')
const ECONOMIC_BOWLER_IN_SUPER_OVER = path.join(__dirname, '../public/output/bestEconomyInSuperOver.json')
const HTML_FILE = path.join(__dirname, '../public/index.html')
const CSS_FILE = path.join(__dirname, '../public/style.css')
const APP_JS_FILE = path.join(__dirname, '../public/app.js')
const pathFind = path.join(__dirname, "..")

let port = 8000

let convertToJSON = (csvPath) => {
    return csv().fromFile(csvPath)
}

Promise.all([convertToJSON(MATCHES_PATH), convertToJSON(DELIVERIES_PATH)])
    .then(([matches, deliveries]) => {

        let strikeRate = ipl.strikeRate(matches, deliveries)
        saveToJson(strikeRate, STRIKE_RATE_EACH_YEAR)

        let tossWinner = ipl.tossAndWin(matches)
        saveToJson(tossWinner, TEAMS_WINNING_TOSS_AND_MATCH)

        let playerOfMatch = ipl.playerOfTheMatch(matches)
        saveToJson(playerOfMatch, PLAYER_OF_THE_MATCH)

        let dismiss = ipl.playerDismissed(deliveries)
        saveToJson(dismiss, PLAYER_DISMISSED)

        let economy = ipl.economicBowlerInSuperOver(deliveries)
        saveToJson(economy, ECONOMIC_BOWLER_IN_SUPER_OVER)
    }).catch(console.log)

// function which save result to given path
function saveToJson(result, path) {
    const jsonData = result
    const jsonString = JSON.stringify(jsonData);
    fs.writeFile(path, jsonString, err => {
        if (err) {
            console.error(err);
        }
    });
}

let server = http.createServer((req, res) => {
    let path = req.url
    switch (path) {
        case "/":
            fs.readFile(HTML_FILE, (err, html) => {
                if (err) { console.log(err) }
                else {
                    res.writeHead(200, { "Content-type": "text/html" })
                    res.write(html)
                    res.end()
                }
            });
            break;
        case "/style.css":
            fs.readFile(CSS_FILE, (err, css) => {
                if (err) { console.log(err) }
                else {
                    res.writeHead(200, { "Content-type": "text/css" })
                    res.write(css)
                    res.end()
                }
            });
            break;
        case "/app.js":
            fs.readFile(APP_JS_FILE, (err, app) => {
                if (err) { console.log(err) }
                else {
                    res.writeHead(200, { "Content-type": "application/javascript" })
                    res.write(app)
                    res.end()
                }
            });
            break;
        default:
            let url = pathFind + req.url
            if (url.includes(".json")) {
                fs.readFile(url, (err, data) => {
                    if (err) { console.log(err) }
                    else {
                        res.writeHead(200, { "Content-type": "application/json" })
                        res.write(data)
                        res.end()
                    }
                })
            } else {
                res.writeHead(200, { "Content-type": "text/html" })
                res.write('<h1>BAD REQUEST</h1>')
                res.end()
            }

    }
})

server.listen(port, () => { console.log(`Listening on port :${port} ... `) })